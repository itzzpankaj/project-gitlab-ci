# syntax=docker/dockerfile:1

# NODE BLOCK
# Fetching the node image from docker hub
FROM node:18-alpine  as builder

# Setting the working directory
WORKDIR /cicd

# Copying the package.json file
COPY package*.json ./

# Installing the dependencies
RUN yarn install

# Copying the rest of the files
COPY . .

# Building the app
RUN yarn build

# NGINX BLOCK
# Fetching the nginx image from docker hub
FROM nginx:1.23-alpine

## Remove default nginx index page
RUN rm -rf /usr/share/nginx/html/*
COPY nginx.conf /etc/nginx/conf.d/default.conf 

# Copy from the stahg 1
COPY --from=builder /cicd/build /usr/share/nginx/html

# Expose port
EXPOSE 80

# SET THE ENTRYPOINT
ENTRYPOINT ["nginx", "-g", "daemon off;"]

# To build the image
# docker build -t cicd:1.0.0 .

# To run the image
# docker run -d -p 80:80 --name cicd cicd:1.0.0
